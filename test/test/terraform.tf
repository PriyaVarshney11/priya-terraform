terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}
provider "aws" {
  region     = "us-east-1"
  access_key = "AKIAVXWAVT5RCVPEXND7"
  secret_key = "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"
}
resource "aws_iam_role" "ansible-role" {
  name = "ansible-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_iam_instance_profile" "ansible-role" {
  name = "ansible-role"
  role = aws_iam_role.ansible-role.name
}
resource "aws_iam_role_policy" "ansible-role-full_access" {
  name = "ansible-role-full_access"
  role = aws_iam_role.ansible-role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "*",
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_instance" "ansible-role" {
  ami           = "ami-00874d747dde814fa"
  instance_type = "t2.micro"
  key_name      = "gold"
  iam_instance_profile = aws_iam_instance_profile.ansible-role.name
  tags = {
    Name = "terraform-instance"
  }
user_data =  "${file("user-data.sh")}"
}

